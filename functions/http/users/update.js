import { success, failure } from '../../../libs/response-lib';
import * as dynamoDbLib from '../../../libs/dynamodb-lib';
import * as UserService from '../../../services/user/UserServices';

export async function main (event, context, callback) {
  const timestamp = Date.now();
  const data = event.mock ? event.body : JSON.parse(event.body);

  const user = await UserService.getById(event.pathParameters.userId);
  if (!user) {
    return failure(
      {
        error: {
          message: 'No user with ID ' + event.pathParameters.userId
        }
      },
      404
    );
  }

  try {
    await dynamoDbLib.call('update', {
      TableName: process.env.usersTableName,
      Key: {
        id: event.pathParameters.userId
      },
      UpdateExpression: 'set updated = :meta, firstName = :firstName, lastName = :lastName, birthDay = :birthDay, subscribedToNewsletter = :subscribedToNewsletter',
      ExpressionAttributeValues: {
        ':lastName': data.lastName,
        ':firstName': data.firstName,
        ':birthDay': data.birthDay,
        ':subscribedToNewsletter': data.subscribedToNewsletter,
        ':meta': timestamp
      }
    });
    return success({ id: event.pathParameters.userId });
  } catch (e) {
    return failure({
      error: {
        message: e,
        fields: '',
        stack: e.stack
      }
    });
  }
}
