import { success, failure } from '../../../libs/response-lib';
import * as UserServices from '../../../services/user/UserServices';

export async function main (event, context, callback) {
  const user = await UserServices.getById(event.pathParameters.userId);
  if (!user) {
    return failure(
      {
        error: {
          message: 'No user with ID ' + event.pathParameters.userId
        }
      },
      404
    );
  } else {
    return success(user, 200);
  }
}
