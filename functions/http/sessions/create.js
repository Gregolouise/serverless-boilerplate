import * as dynamoDbLib from '../../../libs/dynamodb-lib';
import { success, failure } from '../../../libs/response-lib';
import uuid from 'uuid';

export async function main (event, context, callback) {
  const data = JSON.parse(event.body);
  // const data = event.body;
  const identityId = event.requestContext.identity.cognitoIdentityId;

  const userIdentity = await dynamoDbLib.call('get', {
    TableName: process.env.usersIdentitiesTableName,
    Key: {
      identityId: identityId
    }
  });
  const userId = uuid.v1();
  const creationDate = Date.now();
  if (userIdentity.Item === undefined) {
    const userIdentityUser = await dynamoDbLib.call('query', {
      TableName: process.env.usersIdentitiesTableName,
      IndexName: 'email',
      KeyConditionExpression: 'email = :email',
      ExpressionAttributeValues: {
        ':email': data.email
      }
    });
    if (userIdentityUser.Items.length === 0) {
      try {
        const userParams = {
          TableName: process.env.usersTableName,
          Item: {
            id: userId,
            email: data.email,
            meta: {
              created: creationDate,
              updated: creationDate,
              type: 'user',
              version: 1
            }
          }
        };
        await dynamoDbLib.call('put', userParams);
        await dynamoDbLib.call('put', {
          TableName: process.env.usersIdentitiesTableName,
          Item: {
            identityId: identityId,
            userId: userId,
            email: data.email,
            meta: {
              created: creationDate,
              updated: creationDate,
              type: 'userIdentity',
              version: 1
            }
          }
        });

        return success({ user: userParams.Item, firstLogin: true });
      } catch (error) {
        return failure({ error: error });
      }
    } else {
      const userNewParams = {
        TableName: process.env.usersIdentitiesTableName,
        Item: {
          userId: userIdentityUser.Items[0].userId,
          identityId: identityId,
          email: data.email,
          meta: {
            created: creationDate,
            updated: creationDate,
            type: 'userIdentity',
            version: 1
          }
        }
      };
      await dynamoDbLib.call('put', userNewParams);
      const user = await dynamoDbLib.call('get', {
        TableName: process.env.usersTableName,
        Key: {
          id: userIdentityUser.Items[0].userId
        }
      });
      return success({ user: user.Item, firstLogin: false });
    }
  } else {
    if (userIdentity.Item.email === data.email) {
      const user = await dynamoDbLib.call('get', {
        TableName: process.env.usersTableName,
        Key: {
          id: userIdentity.Item.userId
        }
      });
      return success({ user: user.Item, firstLogin: false });
    } else {
      return failure({ error: 'Email address not found !' }, 404);
    }
  }
}
