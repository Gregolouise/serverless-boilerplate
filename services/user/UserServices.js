import * as dynamoDbLib from './../../libs/dynamodb-lib';

export async function getById (id) {
  const result = await dynamoDbLib.call('get', {
    TableName: process.env.usersTableName,
    Key: {
      id: id
    }
  });

  console.log('check user by id', result);

  return result.Item;
}
