export function success (body, statusCode) {
  return buildResponse(statusCode || 200, body);
}

export function failure (body, errorCode) {
  return buildResponse(errorCode || 500, body);
}

function buildResponse (statusCode, body) {
  return {
    statusCode: statusCode,
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Credentials': true
    },
    body: statusCode === 204 ? '' : JSON.stringify(body || {})
  };
}
